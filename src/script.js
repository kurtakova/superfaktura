$(document).ready(function () {
  // DESKTOP MENU
  const tabs = $(".tab");
  const tabContents = $(".tab-content");

  function toggleTab(tab) {
    const target = $(tab).data("target");
    tabContents.addClass("hidden");
    tabs.removeClass("tab-active");
    $(tab).addClass("tab-active");

    $("#" + target)
      .removeClass("hidden")
      .find(".submenu-item")
      .removeClass("active")
      .first()
      .addClass("active");
  }

  tabs.on("click", function () {
    toggleTab(this);
  });

  $(".submenu-item").click(function () {
    $(".submenu-item").removeClass("active");
    $(this).addClass("active");
  });

  // MOBILE MENU
  $("#open-menu").click(function () {
    $("#mobile-menu").slideDown();
  });

  $("#close-menu").click(function () {
    $("#mobile-menu").slideUp();
  });
});
