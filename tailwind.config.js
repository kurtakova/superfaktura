/** @type {import("tailwindcss").Config} */
module.exports = {
  content: ["*.{html,js}"],
  theme: {
    fontFamily: {
      Mukta: ["Mukta", "sans-serif"],
    },
    extend: {
      colors: {
        piktogram: {
          magenta: "#E03BBC",
          cyan: "#00B8F0",
          orange: "#FFBC03",
        },
        seduction_green: {
          100: "#48A332",
        },
        bracing_blue: {
          100: "#003A7F",
          50: "#809CBF",
        },
        enterprise_gray: {
          10: "#F2F2F4",
          30: "#D0D4DD",
          80: "#8394A3",
        },
        star_blue: {
          100: "#0A7EC7",
        },
        atlantic_black: {
          100: "#293040",
        },
      },
      container: {
        padding: {
          DEFAULT: "1rem",
        },
      },
      boxShadow: {
        menu: "0px 10px 20px 0px rgba(100, 121, 140, 0.1)",
      },
    },
  },
  plugins: [],
};
